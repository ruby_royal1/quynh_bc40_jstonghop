let getEl = (id) => {
    return document.getElementById(id);
}

// 1. IN BẢNG CÓ 100 PHẦN TỬ

function inBang() {
    getEl("tb1").innerHTML = "";
    var a = "";
    for (let h = 1; h < 101; h = h + 10) {
        a += `<tr>`;
        for (let c = h; c < h + 10; c++) {
            // console.log(c);
            a += `
        <td>${c}</td>
        `;
        }; a += `</tr>`;

    }
    return getEl("tb1").innerHTML = a;
};
inBang();


// BÀI 2: TÌM SỐ NGUYÊN TỐ TRONG MẢNG
arr = [];
primeArr = [];
function addNum() {
    var num = getEl("num-x").value * 1;
    arr.push(num);
    getEl("txt-2-result").innerText = "";
    primeArr = [];
    return getEl("txt-arr-2").innerText = arr;

};


function printPrime() {

    for (let i = 0; i < arr.length; i++) {
        arr[i];

        if (checkPrime(arr[i])) {

            primeArr.push(arr[i]);

        } else {

        }

    };

    getEl("txt-2-result").innerText = primeArr;
};


// BÀI 3: TÍNH TỔNG

function tinhTong() {
    var a = getEl("num-3-n").value * 1;
    var s = 0;
    var sum = 0;
    for (let i = 2; i < a + 1; i++) {
        s += i;
        sum = s + 2 * a;
    };
    return getEl("txt-3-result").innerText = sum;
};

// BÀI 4:TÌM ƯỚC CỦA 1 SỐ NGUYÊN
uoc = [];
function timUoc() {
    var a = getEl("num-4-n").value * 1;
    uoc = [a];
    for (let i = 1; i < (a / 2 + 1); i++) {
        if ((a % i) == 0) {
            uoc.push(i);
        };

    }
    return getEl("txt-4-result").innerText = uoc.sort(function (a, b) { return a - b });
}

// BÀI 5: IN SỐ ĐẢO NGƯỢC
function daoNguoc() {
    var a = getEl("num-5-n").value;
    let char = [...a];
    let revChar = char.reverse();
    return getEl("txt-5-result").innerText = revChar.join("");
}

// BÀI 6: TÌM N NGUYÊN DƯƠNG N(N+1)/2<=100
function timN() {
    var count = 1;
    var sum = 0;
    var temp = 0;

    for (count = 1; count < 100; count++) {
        sum += count;
        if ((sum) > 100) {
            temp = count;
            break;
        }
    }
    document.getElementById("txt-kq").innerHTML = `Số n cần tìm là: ${temp}`;
}

// BÀI 7: IN BẢNG CỬU CHƯƠNG
function inCuuChuong() {
    var n = getEl("num-7-n").value * 1;
    getEl("txt-7-result").innerHTML = "";
    for (let i = 0; i < 11; i++) {
        getEl("txt-7-result").innerHTML += `
        ${n + 'x' + i + '=' + (n * i)} </br>
        `;

    };
};

// BÀI 8: CHIA BÀI
var players = [[], [], [], []];
var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];

function chiaBai() {
    for (let i = 0; i < cards.length; i++) {
        if ((i + 1) % 4 == 1) {
            players[0].push(cards[i]);
        } else if ((i + 1) % 4 == 2) {
            players[1].push(cards[i]);
        } else if ((i + 1) % 4 == 3) {
            players[2].push(cards[i]);
        } else {
            players[3].push(cards[i]);
        };


    }
    var player1 = players[0];
    var player2 = players[1];
    var player3 = players[2];
    var player4 = players[3];

    getEl("txt-8-kq").innerHTML = `
    player1=  ${players[0]} </br> 
    player2=  ${players[1]} </br> 
    player3=  ${players[2]} </br> 
    player4=  ${players[3]} </br> 
    `

}

// BÀI 9: TÍNH GÀ TÍNH CHÓ 

function tinhGaCho() {
    var m = getEl("num-9-m").value * 1;
    var n = getEl("num-9-n").value * 1;
    var ga = (m * 4 - n * 1) / 2;
    var cho = (1 * n - 2 * m) / 2;
    if (Number.isInteger(ga) && ga > 0 && Number.isInteger(cho) && cho > 0) {
        getEl("txt-9-result").innerHTML = `
    Số gà là: ${ga} </br> Số chó là: ${cho}
    `
    } else {
        getEl("txt-9-result").innerHTML = `
    Không có con gà con chó nào thỏa mãn điều kiện đó đâu!
    `
    }
}

// BÀI 10: TÍNH GÓC LỆCH GIỮA KIM GIỜ VÀ KIM PHÚT
var angleClock = (h, min) => {
    const minAngle = min * 6;
    const hAngle = (h * 30) + (min * 0.5);
    const angle = Math.abs(hAngle - minAngle);
    return Math.min(angle, 360 - angle);
}

function calDeg() {
    var h = getEl("num-10-m").value * 1;
    var mi = getEl("num-10-n").value * 1;

    getEl("txt-10-result").innerHTML = `
    Góc giữa kim giờ và kim phút là: ${angleClock(h, mi) < 0 ? 360 + angleClock(h, mi) : angleClock(h, mi)} độ
   
    `

}



function checkPrime(a) {
    if (a < 2) {
        return false;
    } else if (a == 2) {
        return true;
    }
    else if (a % 2 == 0) {
        return false;
    }
    else {
        for (let i = 3; i <= Math.sqrt(a); i += 2) {
            if (a % i == 0) {
                return false;
            }
        }
    } return true;

}



